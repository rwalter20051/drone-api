INSERT INTO drones (id, serial_number, model, weight_limit, battery_capacity, state) VALUES (1, 'A-100', 'Lightweight', 300, 75, 'IDLE');
INSERT INTO drones (id, serial_number, model, weight_limit, battery_capacity, state) VALUES (2, 'B-100', 'Middleweight', 400, 86, 'LOADING');
INSERT INTO drones (id, serial_number, model, weight_limit, battery_capacity, state) VALUES (3, 'C-100', 'Cruiserweight', 450, 23, 'IDLE');
INSERT INTO drones (id, serial_number, model, weight_limit, battery_capacity, state) VALUES (4, 'D-100', 'Heavyweight', 500, 80, 'LOADED');
INSERT INTO drones (id, serial_number, model, weight_limit, battery_capacity, state) VALUES (5, 'E-100', 'Lightweight', 300, 50, 'IDLE');

INSERT INTO medications (id, name, weight, code, image) VALUES (1, 'Panadol', 60, 'MED_100', 'panadol.png');
INSERT INTO medications (id, name, weight, code, image) VALUES (2, 'Aspirin', 100, 'MED_101', 'Aspirin.png');
INSERT INTO medications (id, name, weight, code, image) VALUES (3, 'Cetirizine', 90, 'MED_102', 'Cetirizine.png');
INSERT INTO medications (id, name, weight, code, image) VALUES (4, 'Diclofenac', 50, 'MED_103', 'Diclofenac.png');
INSERT INTO medications (id, name, weight, code, image) VALUES (5, 'Ibuprofen', 80, 'MED_104', 'Ibuprofen.png');
INSERT INTO medications (id, name, weight, code, image) VALUES (6, 'Meclizine', 100, 'MED_105', 'Meclizine.png');

/*INSERT INTO trips (id, created_at, drone_id) VALUES  (1, '2022-12-18 16:34', 4);
INSERT INTO trips (id, created_at, drone_id) VALUES  (2, '2022-12-18 16:35', 2);

INSERT INTO trip_medication (id, quantity, medication_id, trip_id) VALUES (1, 2, 2, 1);
INSERT INTO trip_medication (id, quantity, medication_id, trip_id) VALUES (2, 3, 1, 1);
INSERT INTO trip_medication (id, quantity, medication_id, trip_id) VALUES (3, 1, 5, 1);

INSERT INTO trip_medication (id, quantity, medication_id, trip_id) VALUES (4, 2, 3, 2);
INSERT INTO trip_medication (id, quantity, medication_id, trip_id) VALUES (5, 2, 6, 2);
INSERT INTO trip_medication (id, quantity, medication_id, trip_id) VALUES (6, 1, 2, 2);
*/