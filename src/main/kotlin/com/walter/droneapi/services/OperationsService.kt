package com.walter.droneapi.services

import com.walter.droneapi.dto.BatteryLevelDTO
import com.walter.droneapi.dto.LoadDroneDTO
import com.walter.droneapi.dto.LoadedMedicationItemDTO
import com.walter.droneapi.entities.Drone
import com.walter.droneapi.entities.Trip
import com.walter.droneapi.entities.TripMedication
import com.walter.droneapi.enums.DroneState
import com.walter.droneapi.exceptions.InvalidValuesException
import com.walter.droneapi.repository.DroneRepository
import com.walter.droneapi.repository.MedicationRepository
import com.walter.droneapi.repository.TripMedicationRepository
import com.walter.droneapi.repository.TripRepository
import org.springframework.stereotype.Service
import java.lang.IllegalStateException
import java.time.LocalDateTime

@Service
class OperationsService(
    val droneRepository: DroneRepository,
    val medicationRepository: MedicationRepository,
    val tripRepository: TripRepository,
    val tripMedicationRepository: TripMedicationRepository
) {
    fun loadDroneWithMedicalItems(loadDroneDTO: LoadDroneDTO) {
        val medItems = loadDroneDTO.medicationIdList
        val droneId = loadDroneDTO.droneId
        val droneOptional = droneRepository.findById(droneId)
        if (!droneOptional.isPresent) {
            throw InvalidValuesException("Drone ID is invalid")
        }
        val drone = droneOptional.get()

        if (drone.state != DroneState.IDLE) {
            throw InvalidValuesException("You can only load drones that are in idle state. Current drone state is ${drone.state}")
        }

        if (drone.batteryCapacity < 25) {
            throw InvalidValuesException("Battery capacity is too low  ${drone.batteryCapacity} %")
        }

        var drugWeight: Double = 0.0
        for (item in medItems) {
            val medicationOptional = medicationRepository.findById(item.medicationId)
            if (!medicationOptional.isPresent) {
                throw InvalidValuesException("Invalid medication Item ID ${item.medicationId}")
            }
            drugWeight += item.quantity.toDouble() * medicationOptional.get().weight
        }

        drone.state = DroneState.LOADING
        droneRepository.save(drone)

        //Create trip info
        val trip = Trip(null, LocalDateTime.now(), drone)
        tripRepository.save(trip)

        //add medication items
        for (item in medItems) {
            val medication = medicationRepository.findById(item.medicationId).get()
            val tripMedication = TripMedication(null, item.quantity, trip, medication)
            tripMedicationRepository.save(tripMedication)
        }
    }

    fun getAvailableDrones(): List<Drone> {
        return droneRepository.findByState(DroneState.IDLE).filter { it.batteryCapacity > 25 }
    }

    fun getDroneBatteryLevel(droneId: Long): BatteryLevelDTO {
        val droneOptional = droneRepository.findById(droneId)
        if (!droneOptional.isPresent) {
            throw InvalidValuesException("Drone ID is invalid")
        }
        val drone = droneOptional.get()
        val droneBatteryInfo = drone.let {
            BatteryLevelDTO(it.id!!, it.batteryCapacity)
        }
        return droneBatteryInfo
    }

    fun getLoadedMedicationsOnADrone(droneId: Long): MutableList<LoadedMedicationItemDTO> {
        if (!droneRepository.existsById(droneId)) {
            throw InvalidValuesException("Drone ID is invalid")
        }
        val tripOptional = tripRepository.getTripByDroneId(droneId)
        if (!tripOptional.isPresent) {
            throw InvalidValuesException("Could not find any medication items for drone $droneId")
        }
        val trip = tripOptional.get()
        val tripMedications = trip.tripMedications
        val medicationList  = mutableListOf<LoadedMedicationItemDTO>()
        for (tripMedication in tripMedications){
            val medication = tripMedication.medication
            val loadedItem = medication!!.id?.let {
                LoadedMedicationItemDTO(
                    it,
                    medication.name,
                    medication.weight,
                    medication.code,
                    tripMedication.quantity
                )
            }
            if (loadedItem != null) {
                medicationList.add(loadedItem)
            }
        }
        return medicationList;
    }
}