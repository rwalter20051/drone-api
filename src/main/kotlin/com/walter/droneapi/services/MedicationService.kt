package com.walter.droneapi.services

import com.walter.droneapi.dto.MedicationDTO
import com.walter.droneapi.entities.Medication
import com.walter.droneapi.exceptions.InvalidValuesException
import com.walter.droneapi.repository.MedicationRepository
import org.springframework.stereotype.Service

@Service
class MedicationService(val medicationRepository: MedicationRepository) {
    fun addMedication(medicationDTO: MedicationDTO): MedicationDTO {
        val medicationOptional = medicationRepository.findMedicationByCode(medicationDTO.code)
        if (medicationOptional.isPresent) {
            throw InvalidValuesException("Another Medication is already using this code ${medicationDTO.code}")
        }

        val medicationEntity = medicationDTO.let {
            Medication(null, it.name, it.weight, it.code,it.image)
        }

        medicationRepository.save(medicationEntity)

        return  medicationEntity.let {
            MedicationDTO(it.id, it.name, it.weight, it.code, it.image)
        }

    }

    fun getAllMedicationsAvailable():List<Medication>{
       return  medicationRepository.findAll()
    }
}