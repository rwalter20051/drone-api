package com.walter.droneapi.services

import com.walter.droneapi.dto.BatteryLevelDTO
import com.walter.droneapi.dto.DroneDTO
import com.walter.droneapi.dto.DroneStateDTO
import com.walter.droneapi.entities.Drone
import com.walter.droneapi.exceptions.InvalidValuesException
import com.walter.droneapi.repository.DroneRepository
import org.springframework.stereotype.Service

@Service
class DroneService(val droneRepository: DroneRepository) {
    fun addDrone(droneDTO: DroneDTO): DroneDTO {
       val droneOptional = droneRepository.findBySerialNumber(droneDTO.serialNumber)
       if (droneOptional.isPresent){
           throw InvalidValuesException("Another drone is already using this serial number ${droneDTO.serialNumber}")
       }

        val droneEntity = droneDTO.let {
            Drone(null, it.serialNumber, it.model, it.weightLimit, it.batteryCapacity, it.state)
        }
        droneRepository.save(droneEntity)
        return  droneEntity.let {
            DroneDTO(it.id, it.serialNumber, it.model, it.weightLimit, it.batteryCapacity, it.state)
        }
    }

    fun  updateBattery(droneBatteryLevelDTO: BatteryLevelDTO): Drone {
        val droneOptional = droneRepository.findById(droneBatteryLevelDTO.droneId)
        if (!droneOptional.isPresent){
            throw InvalidValuesException("Could not find a drone with id  ${droneBatteryLevelDTO.droneId}")
        }
        val drone = droneOptional.get()
        drone.batteryCapacity = droneBatteryLevelDTO.batteryLevel
        droneRepository.save(drone)
        return drone
    }

    fun updateDroneState(droneStateDTO: DroneStateDTO): Drone {
        val droneOptional = droneRepository.findById(droneStateDTO.droneId)
        if (!droneOptional.isPresent){
            throw InvalidValuesException("Could not find a drone with id  ${droneStateDTO.droneId}")
        }
        val drone = droneOptional.get()
        drone.state = droneStateDTO.state
        droneRepository.save(drone)
        return drone
    }
}