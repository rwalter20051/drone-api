package com.walter.droneapi.controllers

import com.walter.droneapi.dto.MedicationDTO
import com.walter.droneapi.entities.Medication
import com.walter.droneapi.repository.MedicationRepository
import com.walter.droneapi.services.MedicationService
import jakarta.validation.Valid
import org.springframework.http.HttpStatus
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api/v1/medications")
@Validated
class MedicationController(val medicationService: MedicationService) {

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun addMedication(@RequestBody @Valid medicationDto: MedicationDTO): MedicationDTO {
        return medicationService.addMedication(medicationDto)
    }

    @GetMapping
    fun getAllMedications(): List<Medication> {
        return medicationService.getAllMedicationsAvailable()
    }
}