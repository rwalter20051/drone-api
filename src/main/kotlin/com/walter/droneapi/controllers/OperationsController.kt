package com.walter.droneapi.controllers

import com.walter.droneapi.dto.BatteryLevelDTO
import com.walter.droneapi.dto.LoadDroneDTO
import com.walter.droneapi.dto.LoadedMedicationItemDTO
import com.walter.droneapi.entities.Drone
import com.walter.droneapi.services.OperationsService
import jakarta.validation.Valid
import org.springframework.http.HttpStatus
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/v1/operations")
@Validated
class OperationsController(val operationsService: OperationsService) {

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun loadDrone(@RequestBody @Valid loadDroneDTO: LoadDroneDTO) {
       operationsService.loadDroneWithMedicalItems(loadDroneDTO)
    }

    @GetMapping("/battery/{droneId}")
    fun getDroneBattery(@PathVariable droneId: Long): BatteryLevelDTO {
        return operationsService.getDroneBatteryLevel(droneId)
    }

    @GetMapping("/loadedItems/{droneId}")
    fun getLoadedMedicationsOnADrone(@PathVariable droneId: Long): MutableList<LoadedMedicationItemDTO> {
         return operationsService.getLoadedMedicationsOnADrone(droneId)
    }

    @GetMapping("/available/drones")
    fun getAvailableDrones(): List<Drone> {
        return operationsService.getAvailableDrones()
    }
}