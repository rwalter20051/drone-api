package com.walter.droneapi.controllers

import com.walter.droneapi.dto.BatteryLevelDTO
import com.walter.droneapi.dto.DroneDTO
import com.walter.droneapi.dto.DroneStateDTO
import com.walter.droneapi.entities.Drone
import com.walter.droneapi.services.DroneService
import jakarta.validation.Valid
import org.springframework.http.HttpStatus
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/v1/drones")
@Validated
class DroneController(val droneService: DroneService) {

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun addDrone(@RequestBody @Valid droneDTO: DroneDTO) : DroneDTO{
        return  droneService.addDrone(droneDTO)
    }

    @PutMapping("/battery")
    fun updateBattery(@RequestBody @Valid droneBatteryLevelDTO: BatteryLevelDTO): Drone {
        return droneService.updateBattery(droneBatteryLevelDTO)
    }

    @PutMapping("/state")
    fun updateDroneState(@RequestBody @Valid droneStateDTO: DroneStateDTO): Drone {
        return droneService.updateDroneState(droneStateDTO)
    }
}