package com.walter.droneapi.entities

import jakarta.persistence.*

@Entity
@Table(name = "trip_medication")
data class TripMedication (
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long?,

    @Column(nullable = false)
    var quantity: Int=1,

    @ManyToOne
    @JoinColumn(name = "trip_id")
    var  trip: Trip,

    @ManyToOne
    @JoinColumn(name = "medication_id", nullable = false)
    var medication: Medication? = null
)
{
    override fun toString(): String {
        return "TripMedication(id=$id, quantity=$quantity, trip=$trip, medication=${medication!!.id})"
    }
}