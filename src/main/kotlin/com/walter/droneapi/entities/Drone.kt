package com.walter.droneapi.entities

import com.walter.droneapi.enums.DroneModel
import com.walter.droneapi.enums.DroneState
import jakarta.persistence.*

@Entity
@Table(name = "drones")
data class Drone (
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long?,
    @Column(nullable = false, unique = true)
    var serialNumber: String,
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    var model: DroneModel,
    @Column(nullable = false)
    var weightLimit: Double,
    @Column(nullable = false)
    var batteryCapacity: Int,
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    var state: DroneState,

){
}