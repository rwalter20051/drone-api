package com.walter.droneapi.entities

import com.fasterxml.jackson.annotation.JsonIgnore
import jakarta.persistence.*


@Entity
@Table(name = "medications")
data class Medication(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long?,
    @Column(nullable = false)
    var name: String,
    @Column(nullable = false)
    var weight: Double,
    @Column(nullable = false, unique = true)
    var code: String,
    @Column(nullable = false)
    var image: String,

    @JsonIgnore
    @OneToMany(mappedBy = "medication", fetch = FetchType.LAZY)
    var tripMedications: List<TripMedication> = mutableListOf()
) {

}