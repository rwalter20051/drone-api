package com.walter.droneapi.entities

import jakarta.persistence.*
import org.hibernate.Hibernate
import java.time.LocalDateTime

@Entity
@Table(name = "trips")
data class Trip(
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    val id: Long?,

    @Column(nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    val createdAt: LocalDateTime,

    @OneToOne
    @JoinColumn(name = "drone_id")
    val drone : Drone,

    @OneToMany(mappedBy = "trip", fetch = FetchType.LAZY, cascade = [CascadeType.ALL], orphanRemoval = true)
    var tripMedications: List<TripMedication> = mutableListOf()
) {
    override fun toString(): String {
        return "Trip(id=$id, createdAt=$createdAt, drone=$drone, tripMedications=$tripMedications)"
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other == null || Hibernate.getClass(this) != Hibernate.getClass(other)) return false
        other as Trip

        return id != null && id == other.id
    }

    override fun hashCode(): Int = javaClass.hashCode()
}