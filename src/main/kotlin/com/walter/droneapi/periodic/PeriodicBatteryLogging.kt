package com.walter.droneapi.periodic

import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper
import com.walter.droneapi.dto.BatteryLevelDTO
import com.walter.droneapi.repository.DroneRepository
import mu.KLogging
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component

@Component
class PeriodicBatteryLogging(val droneRepository: DroneRepository) {
    companion object : KLogging()

    @Scheduled(fixedRate = 60 * 1000, initialDelay = 10 * 1000)
    fun logBatteryLevel() {
        val drones = droneRepository.findAll()
        val batteryLevelDTOS = drones.map { BatteryLevelDTO(it.id!!, it.batteryCapacity) }
        val mapper = ObjectMapper()
        if (batteryLevelDTOS.isEmpty()) {
            return
        }
        try {
            val jsonString = mapper.writeValueAsString(batteryLevelDTOS)
            logger.info(jsonString)
        } catch (ex: JsonProcessingException) {
            ex.printStackTrace()
        }
    }
}