package com.walter.droneapi

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.scheduling.annotation.EnableScheduling

@EnableScheduling
@SpringBootApplication
class DroneApiPracticeApplication

fun main(args: Array<String>) {
    runApplication<DroneApiPracticeApplication>(*args)
}
