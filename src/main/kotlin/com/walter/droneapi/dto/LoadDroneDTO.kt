package com.walter.droneapi.dto

import jakarta.validation.constraints.Min
import jakarta.validation.constraints.NotEmpty

data class LoadDroneDTO(
    @get:Min(1, message = "Provide valid drone ID")
    val droneId : Long,
    @get:NotEmpty(message = "List of medication items ids to be loaded must be provided")
    val medicationIdList: List<MedicationItemDTO>,
) {

}