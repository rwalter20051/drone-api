package com.walter.droneapi.dto

data class LoadedMedicationItemDTO(
    val id :Long,
    var name: String,
    var weight: Double,
    var code: String,
    var quantity: Int
) {
}