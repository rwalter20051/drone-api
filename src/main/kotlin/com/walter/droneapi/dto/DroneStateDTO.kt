package com.walter.droneapi.dto

import com.walter.droneapi.enums.DroneState
import jakarta.validation.constraints.Min
import jakarta.validation.constraints.NotNull

data class DroneStateDTO(
    @get:Min(1, message = "Provide valid drone ID")
    val droneId : Long,
    @get:NotNull(message = "Drone.state must not be null")
    var state: DroneState
    ) {
}