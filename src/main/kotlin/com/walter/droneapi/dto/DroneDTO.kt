package com.walter.droneapi.dto

import com.walter.droneapi.enums.DroneModel
import com.walter.droneapi.enums.DroneState
import jakarta.validation.constraints.Max
import jakarta.validation.constraints.NotNull
import jakarta.validation.constraints.Size

data class DroneDTO(
    val id: Long?,
    @get:Size(max=100, message = "Drone.serialNumber cannot exceed 100 characters")
    var serialNumber: String,
    @get:NotNull(message = "Drone.state must not be null")
    var model: DroneModel,
    @get:Max(500, message = "Drone.weightLimit should be a maximum of 500")
    var weightLimit: Double,
    @get:Max(100, message = "Drone.batteryCapacity should be a maximum of 100")
    var batteryCapacity: Int,
    @get:NotNull(message = "Drone.state must not be null")
    var state: DroneState
) {
}