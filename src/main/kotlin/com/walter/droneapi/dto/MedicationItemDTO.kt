package com.walter.droneapi.dto

import jakarta.validation.constraints.Min

data class MedicationItemDTO(
    @get:Min(1,message = "MedicationItem.medicationId is invalid")
    var medicationId: Long,
    @get:Min(1,message = "MedicationItem.quantity is invalid")
    var quantity: Int,
) {
}