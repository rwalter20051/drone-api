package com.walter.droneapi.dto

import jakarta.validation.constraints.Min
import jakarta.validation.constraints.NotBlank
import jakarta.validation.constraints.NotNull
import jakarta.validation.constraints.Pattern

data class MedicationDTO(
    val id: Long?,
    @get:Pattern(regexp = "^[a-zA-Z0-9_\\-]+$", message = "Medication.name Allowed characters are only letters, - and _ characters")
    var name: String,
    @get: Min(0, message = "Medication.weight cannot be a negative number")
    var weight: Double,
    @get:Pattern(regexp = "^[A-Z0-9_]+$", message = "Medication.code Allowed characters are Uppercase letters, underscores and numbers")
    var code: String,
    @get:NotBlank(message = "Image Path Must Not be blank")
    var image: String
) {
}