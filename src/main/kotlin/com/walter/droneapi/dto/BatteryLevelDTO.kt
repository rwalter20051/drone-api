package com.walter.droneapi.dto

data class BatteryLevelDTO(
    var droneId: Long,
    var batteryLevel: Int,
) {
}