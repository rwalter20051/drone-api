package com.walter.droneapi.enums

enum class DroneModel {
    Lightweight,
    Middleweight,
    Cruiserweight,
    Heavyweight
}