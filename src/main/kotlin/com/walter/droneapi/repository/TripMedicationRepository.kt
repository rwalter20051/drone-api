package com.walter.droneapi.repository

import com.walter.droneapi.entities.TripMedication
import org.springframework.data.jpa.repository.JpaRepository

interface TripMedicationRepository : JpaRepository<TripMedication, Long>{
}