package com.walter.droneapi.repository

import com.walter.droneapi.entities.Drone
import com.walter.droneapi.enums.DroneState
import org.springframework.data.jpa.repository.JpaRepository
import java.util.Optional

interface DroneRepository : JpaRepository<Drone, Long> {
    fun findBySerialNumber(serial: String) : Optional<Drone>
    fun findByState(state: DroneState) : List<Drone>
}