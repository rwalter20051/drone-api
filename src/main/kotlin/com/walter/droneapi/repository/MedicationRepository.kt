package com.walter.droneapi.repository

import com.walter.droneapi.entities.Medication
import org.springframework.data.jpa.repository.JpaRepository
import java.util.Optional

interface MedicationRepository : JpaRepository<Medication, Long>{
    fun findMedicationByCode(code:String) :Optional<Medication>
}