package com.walter.droneapi.repository

import com.walter.droneapi.entities.Trip
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Query
import java.util.Optional

interface TripRepository : JpaRepository<Trip, Long>{

    @Query(value = "SELECT * FROM trips WHERE drone_id=?1 ORDER BY id DESC LIMIT 1", nativeQuery = true)
    fun getTripByDroneId(droneId:Long): Optional<Trip>
}