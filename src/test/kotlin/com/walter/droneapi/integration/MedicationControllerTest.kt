package com.walter.droneapi.integration

import com.walter.droneapi.dto.MedicationDTO
import com.walter.droneapi.entities.Medication
import com.walter.droneapi.repository.MedicationRepository
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.reactive.server.WebTestClient

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@AutoConfigureWebTestClient
class MedicationControllerTest {
    @Autowired
    lateinit var webTestClient: WebTestClient

    @BeforeEach
    fun setUp(){

    }

    @Test
    fun addMedication(){
        //given
        val med = MedicationDTO(null, "Celestamine", 90.0, "MED_112", "Cetirizine.png")
        val responseMed = webTestClient.post()
            .uri("/api/v1/medications")
            .bodyValue(med)
            .exchange()
            .expectStatus().isCreated
            .expectBody(Medication::class.java)
            .returnResult()
            .responseBody
        assertTrue(responseMed!!.id != null)
    }
}