package com.walter.droneapi.integration

import com.walter.droneapi.dto.BatteryLevelDTO
import com.walter.droneapi.dto.DroneDTO
import com.walter.droneapi.dto.DroneStateDTO
import com.walter.droneapi.entities.Drone
import com.walter.droneapi.entities.Medication
import com.walter.droneapi.enums.DroneModel
import com.walter.droneapi.enums.DroneState
import com.walter.droneapi.repository.DroneRepository
import com.walter.droneapi.repository.MedicationRepository
import com.walter.droneapi.repository.TripMedicationRepository
import com.walter.droneapi.repository.TripRepository
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.web.reactive.server.WebTestClient

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
@AutoConfigureWebTestClient
class DroneControllerTest {
    @Autowired
    lateinit var webTestClient: WebTestClient

    @Autowired
    lateinit var droneRepository: DroneRepository

    @Autowired
    lateinit var medicationRepository: MedicationRepository


    
    @BeforeEach
    fun setUp(){
        droneRepository.deleteAll()
        medicationRepository.deleteAll()

        //seed data
        val drone1 = Drone(null, "A-100", DroneModel.Lightweight, 250.0, 75, DroneState.IDLE)
        val drone2 = Drone(null, "A-101", DroneModel.Cruiserweight, 300.0, 24, DroneState.IDLE)
        val drone3 = Drone(null, "A-102", DroneModel.Heavyweight, 500.0, 50, DroneState.IDLE)
        val drone4 = Drone(null, "A-103", DroneModel.Middleweight, 400.0, 65, DroneState.IDLE)
        val drone5 = Drone(null, "A-104", DroneModel.Middleweight, 400.0, 69, DroneState.IDLE)
        
        droneRepository.saveAll(listOf(drone1,drone2,drone3,drone4, drone5))
        
        val med1 = Medication(null, "Panadol", 60.0, "MED_100", "panadol.png")
        val med2 = Medication(null, "Aspirin", 100.0, "MED_101", "Aspirin.png")
        val med3 = Medication(null, "Cetirizine", 90.0, "MED_102", "Cetirizine.png")
        val med4 = Medication(null, "Diclofenac", 50.0, "MED_103", "Diclofenac.png")
        val med5 = Medication(null, "Ibuprofen", 80.0, "MED_104", "Ibuprofen.png")
        val med6 = Medication(null, "Meclizine", 100.0, "MED_105", "Meclizine.png")

        medicationRepository.saveAll(listOf(med1, med2, med3, med4, med5, med6))
    }

    @Test
    fun addDrone(){
        val droneDto = DroneDTO(null, "A-105", DroneModel.Middleweight, 400.0, 77, DroneState.IDLE)
        val droneResponseDTO = webTestClient.post()
            .uri("/api/v1/drones")
            .bodyValue(droneDto)
            .exchange()
            .expectStatus().isCreated
            .expectBody(DroneDTO::class.java)
            .returnResult()
            .responseBody
        assertTrue(droneResponseDTO!!.id != null)
    }

    @Test
    fun updateBattery(){
        //given
        val drone = Drone(null, "A-105", DroneModel.Middleweight, 400.0, 44, DroneState.IDLE)
        droneRepository.save(drone)
        //when
        val droneBatteryDTO = BatteryLevelDTO(drone.id!!, 89)
        val droneResult = webTestClient.put()
            .uri("/api/v1/drones/battery")
            .bodyValue(droneBatteryDTO)
            .exchange()
            .expectStatus().is2xxSuccessful
            .expectBody(Drone::class.java)
            .returnResult()
            .responseBody
        //then
        assertEquals(droneBatteryDTO.batteryLevel, droneResult!!.batteryCapacity)
    }

    @Test
    fun updateState(){
        val drone = Drone(null, "A-108", DroneModel.Heavyweight, 500.0, 87, DroneState.IDLE)
        droneRepository.save(drone)

        //when
        val droneStateDTO = DroneStateDTO(drone.id!!, DroneState.LOADED)
        val droneResult = webTestClient.put()
            .uri("/api/v1/drones/state")
            .bodyValue(droneStateDTO)
            .exchange()
            .expectStatus().is2xxSuccessful
            .expectBody(Drone::class.java)
            .returnResult()
            .responseBody
        //then
        assertEquals(droneStateDTO.state, droneResult!!.state)
    }
}